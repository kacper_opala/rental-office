﻿using RentalOffice.Wcf.DTOs;
using RentalOffice.Wpf.RentalOfficeServices;
using RentalOffice.Wpf.ViewLogic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RentalOffice.Wpf
{
    /// <summary>
    /// Interaction logic for RentalWindow.xaml
    /// </summary>
    public partial class RentalWindow : Window
    {
        private RentalDto rental;

        public RentalDto Rental
        {
            get { return rental; }
            set
            {
                if (rental != value)
                {
                    rental = value;
                    NotifyPropertyChanged("Rental");
                }
            }
        }

        public virtual event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null)
            {
                foreach (string propertyName in propertyNames) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                PropertyChanged(this, new PropertyChangedEventArgs("HasError"));
            }
        }

        bool IsEditMode = true;
        //RentalOfficeServiceClient proxy = new RentalOfficeServiceClient();
        IViewLogicDecorator<RentalDto> rentalsViewLogic =  new ViewLogicDecorator<RentalDto>(new ViewlogicFactory().CreateRentalViewLogic());
        public ObservableCollection<ClientDto> ClientList {get; set;}
        public ObservableCollection<ProductDto> ProductList { get; set; }
        public ObservableCollection<RentalStatusDto> RentalStatusList { get; set; }

        public RentalWindow(RentalDto rentalDto, bool isEditMode)
        {
            InitializeComponent();
            this.IsEditMode = isEditMode;

            ClientList = rentalsViewLogic.GetAllClients();
            ProductList = rentalsViewLogic.GetAllProducts();
            RentalStatusList = rentalsViewLogic.GetAllRentalStatuses();
            ClientsComboBox.ItemsSource = ClientList;
            ProductsComboBox.ItemsSource = ProductList;
            StatusComboBox.ItemsSource = RentalStatusList;

            this.Rental = ToRentalDto(rentalDto);
            this.DataContext = this.Rental;

            ClientsComboBox.SelectedIndex = ClientList.ToList().FindIndex(a => a.Id ==this.Rental.Client.Id);
            ProductsComboBox.SelectedIndex = ProductList.ToList().FindIndex(a => a.Id == this.Rental.Product.Id);
            StatusComboBox.SelectedIndex = RentalStatusList.ToList().FindIndex(a => a.Id == this.Rental.Status.Id);
        }

        private RentalDto ToRentalDto(RentalDto i)
        {
            RentalDto c = new RentalDto();
            c.Id = i.Id;
            c.Client = ToClientDto(i.Client);
            c.Product = ToProductDto(i.Product);
            c.CreationDate = i.CreationDate;
            c.StartRentDate = i.StartRentDate;
            c.EndRentDate = i.EndRentDate;
            c.Status = ToRentalStatusDto(i.Status);
            c.Description = i.Description;
            return c;
        }

        private ClientDto ToClientDto(ClientDto i)
        {
            ClientDto c = new ClientDto();
            c.Id = i.Id;
            c.FirstName = i.FirstName;
            c.LastName = i.LastName;
            c.PhoneNumber = i.PhoneNumber;
            c.EmailAddress = i.EmailAddress;
            c.Description = i.Description;
            return c;

        }

        private RentalStatusDto ToRentalStatusDto(RentalStatusDto i)
        {
            RentalStatusDto c = new RentalStatusDto();
            c.Id = i.Id;
            c.Nazwa = i.Nazwa;
            return c;
        }

        private ProductDto ToProductDto(ProductDto i)
        {
            ProductDto p = new ProductDto();
            p.Id = i.Id;
            p.Name = i.Name;
            p.Description = i.Description;
            return p;                                                     
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsEditMode == false)
                {
                    rentalsViewLogic.Add(Rental);
                    //proxy.AddRental(Rental);
                }
                else
                {
                    rentalsViewLogic.Update(Rental);
                    //proxy.UpdateRental(Rental);
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
