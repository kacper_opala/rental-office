﻿using RentalOffice.Wcf.DTOs;
using RentalOffice.Wpf.RentalOfficeServices;
using RentalOffice.Wpf.ViewLogic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RentalOffice.Wpf
{
    /// <summary>
    /// Interaction logic for ClientWindow.xaml
    /// </summary>
    public partial class ClientWindow : Window
    {
        public ClientDto Client { get; set; }
        bool IsEditMode = true;
        //RentalOfficeServiceClient proxy = new RentalOfficeServiceClient();
        ViewLogicBase<ClientDto> clientViewLogic = new ViewlogicFactory().CreateClientViewLogic();

        public ClientWindow(ClientDto clientDto, bool isEditMode)
        {
            InitializeComponent();
            this.IsEditMode = isEditMode;
            this.Client = clientDto;
            this.DataContext = this.Client;
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            if (IsEditMode == false)
            {
                clientViewLogic.Add(Client);
                //proxy.AddClient(Client);
            }
            else
            {
                clientViewLogic.Update(Client);
                //proxy.UpdateClient(Client);
            }
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
