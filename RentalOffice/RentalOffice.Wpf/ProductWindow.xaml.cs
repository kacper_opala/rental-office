﻿using RentalOffice.Wcf.DTOs;
using RentalOffice.Wpf.RentalOfficeServices;
using RentalOffice.Wpf.ViewLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RentalOffice.Wpf
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class ProductWindow : Window
    {
        public ProductDto Product { get; set; }
        bool IsEditMode = true;
        //RentalOfficeServiceClient proxy = new RentalOfficeServiceClient();
        ViewLogicBase<ProductDto> productViewLogic = new ViewlogicFactory().CreateProductViewLogic();

        public ProductWindow(ProductDto producttDto, bool isEditMode)
        {
            InitializeComponent();
            this.IsEditMode = isEditMode;
            this.Product = producttDto;
            this.DataContext = this.Product;
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsEditMode == false)
                {
                    productViewLogic.Add(Product);
                    //proxy.AddProduct(Product);
                }
                else
                {
                    productViewLogic.Update(Product);
                    //proxy.UpdateProduct(Product);
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
