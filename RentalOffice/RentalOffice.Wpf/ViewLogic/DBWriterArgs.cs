﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wpf.ViewLogic
{
    public class DBWriterArgs : EventArgs
    {
        public string RepositoryName { get; private set; }
        public string MethodName { get; private set; }

        public DBWriterArgs(string repositoryName, string methodName)
        {
            this.RepositoryName = repositoryName;
            this.MethodName = methodName;
        }
    }
}
