﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wpf.ViewLogic
{
    public interface IRepository<Dto>
    {
        Dto GetById(int id);
        ObservableCollection<Dto> GetAll();
        void Add(Dto dto);
        void Delete(Dto dto);
        void Update(Dto dto);
    }
}
