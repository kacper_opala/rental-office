﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wpf.ViewLogic
{
    public class DBWriterConsoleEventHandler
    {
        public void LogTRansaction(object sender, EventArgs eventArgs)
        {
            DBWriterArgs dBWriterArgs = eventArgs as DBWriterArgs;
            Console.WriteLine("Called method " + dBWriterArgs.MethodName + " from repository " + dBWriterArgs.RepositoryName + " that writes to DB.");
        }
    }
}
