﻿using RentalOffice.Wcf.DTOs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wpf.ViewLogic
{
    public class ProductViewLogic : ViewLogicBase<ProductDto>
    {
        public override ProductDto GetById(int id)
        {
            return Proxy.GetProductById(id);
        }

        public override void Add(ProductDto productDto)
        {
            base.Add(productDto);
            Proxy.AddProduct(productDto);
        }

        public override void Delete(ProductDto productDto)
        {
            base.Delete(productDto);
            Proxy.RemoveProduct(productDto);
        }

        public override ObservableCollection<ProductDto> GetAll()
        {
            return Proxy.GetAllProducts();
        }

        public override void Update(ProductDto productDto)
        {
            base.Update(productDto);
            Proxy.UpdateProduct(productDto);
        }

        public ProductViewLogic() : base("Product")
        {

        }
    }
}
