﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentalOffice.Wcf.DTOs;
using RentalOffice.Wpf.RentalOfficeServices;

namespace RentalOffice.Wpf.ViewLogic
{
    public class ViewLogicDecorator<Dto> : IViewLogicDecorator<Dto>
    {
        ViewLogicBase<Dto> _viewLogic;
        protected RentalOfficeServiceClient Proxy { get; set; }

        public ViewLogicDecorator(ViewLogicBase<Dto> viewLogic)
        {
            this._viewLogic = viewLogic;
            this.Proxy = new RentalOfficeServiceClient();
        }

        public void Add(Dto dto)
        {
            _viewLogic.Add(dto);
        }

        public void Delete(Dto dto)
        {
            _viewLogic.Delete(dto);
        }

        public ObservableCollection<Dto> GetAll()
        {
           return _viewLogic.GetAll();
        }

        public ObservableCollection<ClientDto> GetAllClients()
        {
            return Proxy.GetAllClients();
        }

        public ObservableCollection<ProductDto> GetAllProducts()
        {
            return Proxy.GetAllProducts();
        }

        public ObservableCollection<RentalStatusDto> GetAllRentalStatuses()
        {
            return Proxy.GetAllRentalStatuses();
        }

        public Dto GetById(int id)
        {
           return _viewLogic.GetById(id);
        }

        public void Update(Dto dto)
        {
            _viewLogic.Update(dto);
        }
    }
}
