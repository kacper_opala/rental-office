﻿using RentalOffice.Wcf.DTOs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wpf.ViewLogic
{
    public class RentalsViewLogic : ViewLogicBase<RentalDto>
    {
        public override RentalDto GetById(int id)
        {
            return Proxy.GetRentalById(id);
        }

        public override void Add(RentalDto rentalDto)
        {
            base.Add(rentalDto);
            Proxy.AddRental(rentalDto);
        }

        public override void Delete(RentalDto rentalDto)
        {
            base.Delete(rentalDto);
            Proxy.RemoveRental(rentalDto);
        }

        public override ObservableCollection<RentalDto> GetAll()
        {
            return Proxy.GetAllRentals();
        }

        public override void Update(RentalDto rentalDto)
        {
            base.Update(rentalDto);
            Proxy.UpdateRental(rentalDto);
        }

        public RentalsViewLogic() : base("Rental")
        {

        }
    }
}
