﻿using RentalOffice.Wpf.RentalOfficeServices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wpf.ViewLogic
{
    public delegate void DBWriterDelegate(object sender, EventArgs eventArgs);

    public abstract class ViewLogicBase<Dto> : IRepository<Dto>
    {
        public event DBWriterDelegate OnWritingToDBEvent;
        protected RentalOfficeServiceClient Proxy { get; set; }
        public string Name { get; private set; }

        public abstract Dto GetById(int id);
        public abstract ObservableCollection<Dto> GetAll();
        public virtual void Add(Dto dto)
        {
            DBWriterArgs dBWriterArgs = new DBWriterArgs(Name, "Add");
            FireDBWritingEvent(dBWriterArgs);
        }
        public virtual void Delete(Dto dto)
        {
            DBWriterArgs dBWriterArgs = new DBWriterArgs(Name, "Delete");
            FireDBWritingEvent(dBWriterArgs);
        }
        public virtual void Update(Dto dto)
        {
            DBWriterArgs dBWriterArgs = new DBWriterArgs(Name, "Update");
            FireDBWritingEvent(dBWriterArgs);
        }

        protected ViewLogicBase(string name)
        {
            this.Name = name;
            DBWriterConsoleEventHandler handler = new DBWriterConsoleEventHandler();
            OnWritingToDBEvent += handler.LogTRansaction;
            this.Proxy = new RentalOfficeServiceClient();
        }

        private void FireDBWritingEvent(EventArgs eventArgs)
        {
            if (null != OnWritingToDBEvent)
            {
                OnWritingToDBEvent(this, eventArgs);
            }
        }
    }
}
