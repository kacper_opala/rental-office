﻿using RentalOffice.Wcf.DTOs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wpf.ViewLogic
{
    interface IViewLogicDecorator<Dto>  : IRepository<Dto>
    {
        ObservableCollection<ClientDto> GetAllClients();

        ObservableCollection<ProductDto> GetAllProducts();

        ObservableCollection<RentalStatusDto> GetAllRentalStatuses();
    }
}
