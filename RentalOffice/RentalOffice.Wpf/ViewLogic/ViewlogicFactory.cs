﻿using RentalOffice.Wcf.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wpf.ViewLogic
{
    public class ViewlogicFactory
    {
        public ViewLogicBase<ClientDto> CreateClientViewLogic()
        {
            return new ClientViewLogic();
        }

        public ViewLogicBase<ProductDto> CreateProductViewLogic()
        {
            return new ProductViewLogic();
        }

        public ViewLogicBase<RentalDto> CreateRentalViewLogic()
        {
            return new RentalsViewLogic();
        }
    }
}
