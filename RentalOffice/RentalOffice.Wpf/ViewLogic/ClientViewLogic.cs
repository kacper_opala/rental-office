﻿using RentalOffice.Wcf.DTOs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wpf.ViewLogic
{
    public class ClientViewLogic : ViewLogicBase<ClientDto>
    {
        public override ClientDto GetById(int id)
        {
            return Proxy.GetClientById(id);
        }

        public override void Add(ClientDto clientDto)
        {
            base.Add(clientDto);
            Proxy.AddClient(clientDto);
        }

        public override void Delete(ClientDto clientDto)
        {
            base.Delete(clientDto);
            Proxy.RemoveClient(clientDto);
        }

        public override ObservableCollection<ClientDto> GetAll()
        {
            return Proxy.GetAllClients();
        }

        public override void Update(ClientDto clientDto)
        {
            base.Update(clientDto);
            Proxy.UpdateClient(clientDto);
        }

        public ClientViewLogic() : base("Client")
        {

        }
    }
}
