﻿using RentalOffice.Wcf.DTOs;
using RentalOffice.Wpf.RentalOfficeServices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RentalOffice.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            HelloWindow helloWindow = new HelloWindow();
            helloWindow.ShowDialog();
        }

        private void Clients_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClientListWindow clientListWindow = new ClientListWindow();
                clientListWindow.ShowDialog();
                //this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Products_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProductListWindow PrLi = new ProductListWindow();
                PrLi.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Rental_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RentalListWindow RenLi = new RentalListWindow();
                RenLi.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
