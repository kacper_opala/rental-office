﻿using RentalOffice.Wcf.DTOs;
using RentalOffice.Wpf.RentalOfficeServices;
using RentalOffice.Wpf.ViewLogic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RentalOffice.Wpf
{
    /// <summary>
    /// Interaction logic for ProductList.xaml
    /// </summary>
    public partial class ProductListWindow : Window
    {
        public ObservableCollection<ProductDto> ProducTList { get; set; }
        //RentalOfficeServiceClient proxy = new RentalOfficeServiceClient();
        ViewLogicBase<ProductDto> productViewLogic = new ViewlogicFactory().CreateProductViewLogic();

        public ProductListWindow()
        {
            InitializeComponent();
            ProducTList = new ObservableCollection<ProductDto>();
            ProductListView.ItemsSource = new ObservableCollection<ProductDto>(productViewLogic.GetAll());
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProductDto prdto = new ProductDto();
                ProductWindow producttWindow = new ProductWindow(prdto, false);
                producttWindow.ShowDialog();
                ProductListView.ItemsSource = new ObservableCollection<ProductDto>(productViewLogic.GetAll());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProductDto pr = (ProductDto)ProductListView.SelectedItem;
                if (pr == null)
                {
                    return;
                }

                ProductDto p = productViewLogic.GetById(pr.Id);

                ProductWindow producttWindow = new ProductWindow(p, true);
                producttWindow.ShowDialog();
                ProductListView.ItemsSource = new ObservableCollection<ProductDto>(productViewLogic.GetAll());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProductDto prd = (ProductDto)ProductListView.SelectedItem;
                if (prd == null)
                {
                    return;
                }
                var Result = MessageBox.Show(
                    "Do you want to delete this product?!", "Deleting product", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (Result == MessageBoxResult.Yes)
                {
                    productViewLogic.Delete(prd);
                }
                else if (Result == MessageBoxResult.No)
                {
                    return;
                }
                ProductListView.ItemsSource = new ObservableCollection<ProductDto>(productViewLogic.GetAll());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
