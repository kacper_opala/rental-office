﻿using RentalOffice.Wcf.DTOs;
using RentalOffice.Wpf.RentalOfficeServices;
using RentalOffice.Wpf.ViewLogic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RentalOffice.Wpf
{
    /// <summary>
    /// Interaction logic for ItemsList.xaml
    /// </summary>
    public partial class ClientListWindow : Window
    {
        public ObservableCollection<ClientDto> ClientList { get; set; }
        ViewLogicBase<ClientDto> clientViewLogic = new ViewlogicFactory().CreateClientViewLogic();

        public ClientListWindow()
        {
            InitializeComponent();
            ClientList = new ObservableCollection<ClientDto>();
            ClientListView.ItemsSource = new ObservableCollection<ClientDto>(clientViewLogic.GetAll());
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            try
            { 
                ClientDto cldto = new ClientDto();
                //ClientWindow clientWindow = new ClientWindow(new ClientDto());
                ClientWindow clientWindow = new ClientWindow(cldto, false);
                clientWindow.ShowDialog();
                ClientListView.ItemsSource = new ObservableCollection<ClientDto>(clientViewLogic.GetAll());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClientDto cl = (ClientDto)ClientListView.SelectedItem;
                if (cl==null)
                {
                    return;
                }

                ClientDto c = clientViewLogic.GetById(cl.Id);

                ClientWindow clientWindow = new ClientWindow(c, true);
                clientWindow.ShowDialog();
                ClientListView.ItemsSource = new ObservableCollection<ClientDto>(clientViewLogic.GetAll());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClientDto cld = (ClientDto)ClientListView.SelectedItem;
                if (cld == null)
                {
                    return;
                }
                var Result = MessageBox.Show(
                    "Do you want to delete this client?!", "Deleting client", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (Result == MessageBoxResult.Yes)
                {
                    clientViewLogic.Delete(cld);
                    //proxy.RemoveClient(cld);
                }
                else if (Result == MessageBoxResult.No)
                {
                    return;
                }
                ClientListView.ItemsSource = new ObservableCollection<ClientDto>(clientViewLogic.GetAll());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
