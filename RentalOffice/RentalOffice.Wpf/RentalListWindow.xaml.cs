﻿using RentalOffice.Wcf.DTOs;
using RentalOffice.Wpf.RentalOfficeServices;
using RentalOffice.Wpf.ViewLogic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RentalOffice.Wpf
{
    /// <summary>
    /// Interaction logic for RentalList.xaml
    /// </summary>
    public partial class RentalListWindow : Window
    {
        public ObservableCollection<RentalDto> RentaLList { get; set; }
        //RentalOfficeServiceClient proxy = new RentalOfficeServiceClient();
        ViewLogicBase<RentalDto> rentalsViewLogic = new ViewlogicFactory().CreateRentalViewLogic();

        public RentalListWindow()
        {
            InitializeComponent();
            RentaLList = new ObservableCollection<RentalDto>();
            RentalListView.ItemsSource = new ObservableCollection<RentalDto>(rentalsViewLogic.GetAll());
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RentalDto rndto = new RentalDto();
                rndto.Client = new ClientDto();
                rndto.Product = new ProductDto();
                rndto.Status = new RentalStatusDto();

                RentalWindow rentalWindow = new RentalWindow(rndto, false);
                rentalWindow.ShowDialog();
                RentalListView.ItemsSource = new ObservableCollection<RentalDto>(rentalsViewLogic.GetAll());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RentalDto rn = (RentalDto)RentalListView.SelectedItem;
                if (rn == null)
                {
                    return;
                }

                RentalDto r = rentalsViewLogic.GetById(rn.Id);

                RentalWindow rentalWindow = new RentalWindow(r, true);
                rentalWindow.ShowDialog();
                RentalListView.ItemsSource = new ObservableCollection<RentalDto>(rentalsViewLogic.GetAll());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RentalDto rnd = (RentalDto)RentalListView.SelectedItem;
                if (rnd == null)
                {
                    return;
                }
                var Result = MessageBox.Show(
                    "Do you want to delete this rent?!", "Deleting rental", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (Result == MessageBoxResult.Yes)
                {
                    rentalsViewLogic.Delete(rnd);
                    //proxy.RemoveRental(rnd);
                }
                else if (Result == MessageBoxResult.No)
                {
                    return;
                }
                RentalListView.ItemsSource = new ObservableCollection<RentalDto>(rentalsViewLogic.GetAll());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Following execption occured: " + ex.Message, "Exception occured", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
