﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wcf
{
    public class RentalOfficeEntitiesSingleton
    {
        private static RentalOfficeEntities _instance;

        public static RentalOfficeEntities RentalOfficeEntitiesSingletonInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RentalOfficeEntities();
                }
                return _instance;
            }
        }
    }
}
