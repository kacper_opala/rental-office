﻿using RentalOffice.Wcf.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wcf
{
    [ServiceContract]
    public interface IRentalOfficeService
    {
        [OperationContract]
        ProductDto GetProductById(int id);

        [OperationContract]
        IEnumerable<ProductDto> GetAllProducts();

        [OperationContract]
        void AddProduct(ProductDto product);

        [OperationContract]
        void RemoveProduct(ProductDto product);

        [OperationContract]
        void UpdateProduct(ProductDto product);

        [OperationContract]
        ClientDto GetClientById(int id);

        [OperationContract]
        IEnumerable<ClientDto> GetAllClients();

        [OperationContract]
        void AddClient(ClientDto client);

        [OperationContract]
        void RemoveClient(ClientDto client);

        [OperationContract]
        void UpdateClient(ClientDto client);

        [OperationContract]
        RentalDto GetRentalById(int id);

        [OperationContract]
        IEnumerable<RentalDto> GetAllRentals();

        [OperationContract]
        void AddRental(RentalDto rental);

        [OperationContract]
        void RemoveRental(RentalDto rental);

        [OperationContract]
        void UpdateRental(RentalDto rental);

        [OperationContract]
        RentalStatusDto GetRentalStatusById(int id);

        [OperationContract]
        IEnumerable<RentalStatusDto> GetAllRentalStatuses();

        [OperationContract]
        void AddRentalStatus(RentalStatusDto rentalstatus);

        [OperationContract]
        void RemoveRentalStatus(RentalStatusDto rentalstatus);

        [OperationContract]
        void UpdateRentalStatus(RentalStatusDto rentalstatus);
    }
}
