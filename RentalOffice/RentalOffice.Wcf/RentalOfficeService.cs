﻿using RentalOffice.Wcf.DTOs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wcf
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class RentalOfficeService : IRentalOfficeService
    {
        RentalOfficeEntities entities = RentalOfficeEntitiesSingleton.RentalOfficeEntitiesSingletonInstance;

        public ProductDto GetProductById(int id)
        {
            return ToProductDto(entities.Products.FirstOrDefault(i => i.Id == id));
        }

        public IEnumerable<ProductDto> GetAllProducts()
        {
            var list1 = entities.Products.ToList();

            List<ProductDto> list2 = new List<ProductDto>();
            foreach (var i in list1)
            {                                 
                list2.Add(ToProductDto(i));
            }

            return list2;
        }

        public void AddProduct(ProductDto product)
        {
            entities.Products.Add(ToProduct(new Product(), product));
            entities.SaveChanges();
        }
        public void RemoveProduct(ProductDto product)
        {
            var p = entities.Products.First(a => a.Id == product.Id);
            Product pr = ToProduct(p,product);
            entities.Entry(pr).State = EntityState.Deleted;
            entities.SaveChanges();
        }

        public void UpdateProduct(ProductDto product)
        {
            var p = entities.Products.First(a => a.Id == product.Id);
            Product pr = ToProduct(p, product);
            entities.Entry(pr).State = EntityState.Modified;
            entities.SaveChanges();
        }

        private ProductDto ToProductDto(Product i)
        {
            ProductDto p = new ProductDto();
            p.Id = i.Id;
            p.Name = i.Name;
            p.Description = i.Description;
            return p;
        }

        private Product ToProduct(Product product, ProductDto i)
        {
            Product p = product;
            p.Id = i.Id;
            p.Name = i.Name;
            p.Description = i.Description;
            return p;
        }

        public ClientDto GetClientById(int id)
        {
            return ToClientDto(entities.Clients.FirstOrDefault(i => i.Id == id));
        }

        public IEnumerable<ClientDto> GetAllClients()
        {
            var list1 = entities.Clients.AsNoTracking().ToList();

            List<ClientDto> list2 = new List<ClientDto>();
            foreach (var i in list1)
            {
                list2.Add(ToClientDto(i));
            }

            return list2;
        }

        public void AddClient(ClientDto client)
        {
            entities.Clients.Add(ToClient(new Client(), client));
            entities.SaveChanges();
        }

        public void RemoveClient(ClientDto client)
        {
            var c = entities.Clients.First(a => a.Id == client.Id);
            Client cl = ToClient(c, client);
            entities.Entry(cl).State = EntityState.Deleted;
            entities.SaveChanges();
        }

        public void UpdateClient(ClientDto client)
        {
            var c = entities.Clients.First(a => a.Id == client.Id);

            Client cl = ToClient(c, client);
            entities.Entry(cl).State = EntityState.Modified;
            entities.SaveChanges();
        }

        private ClientDto ToClientDto (Client i)
        {
            ClientDto c = new ClientDto();
            c.Id = i.Id;
            c.FirstName = i.FirstName;
            c.LastName = i.LastName;
            c.PhoneNumber = i.PhoneNumber;
            c.EmailAddress = i.EmailAddress;
            c.Description = i.Description;
            return c;
                                   
        }

        private Client ToClient(Client client, ClientDto i)
        {
            Client p = client;
            p.Id = i.Id;
            p.FirstName = i.FirstName;
            p.LastName = i.LastName;
            p.PhoneNumber = i.PhoneNumber;
            p.EmailAddress = i.EmailAddress;
            p.Description = i.Description;
            return p;
        }

        public RentalDto GetRentalById(int id)
        {
            return ToRentalDto(entities.Rentals.FirstOrDefault(i => i.Id == id));
        }

        public IEnumerable<RentalDto> GetAllRentals()
        {
            var list1 = entities.Rentals.ToList();

            List<RentalDto> list2 = new List<RentalDto>();
            foreach (var i in list1)
            {
                list2.Add(ToRentalDto(i));
            }

            return list2;
        }

        public void AddRental(RentalDto rental)
        {
            var c = entities.Clients.First(a => a.Id == rental.Client.Id);
            var p = entities.Products.First(a => a.Id == rental.Product.Id);
            var rs = entities.RentalStatus.First(a => a.Id == rental.Status.Id);

            entities.Rentals.Add(ToRental(new Rental(), c, p, rs, rental));
            entities.SaveChanges();
        }

        public void RemoveRental(RentalDto rental)
        {
            var c = entities.Clients.First(a => a.Id == rental.Client.Id);
            var p = entities.Products.First(a => a.Id == rental.Product.Id);
            var rs = entities.RentalStatus.First(a => a.Id == rental.Status.Id);
            var r = entities.Rentals.First(a => a.Id == rental.Id);

            Rental ren = ToRental(r, c, p, rs, rental);
            entities.Entry(ren).State = EntityState.Deleted;
            entities.SaveChanges();
        }

        public void UpdateRental(RentalDto rental)
        {
            var c = entities.Clients.First(a => a.Id == rental.Client.Id);
            var p = entities.Products.First(a => a.Id == rental.Product.Id);
            var rs = entities.RentalStatus.First(a => a.Id == rental.Status.Id);
            var r = entities.Rentals.First(a => a.Id == rental.Id);

            entities.Entry(ToRental(r, ToClient(c, ToClientDto(c)), ToProduct(p, ToProductDto(p)), ToRentalStatus(rs, ToRentalStatusDto(rs)), rental)).State = EntityState.Modified;
            entities.SaveChanges();
        }

        private RentalDto ToRentalDto(Rental i)
        {
            RentalDto c = new RentalDto();
            c.Id = i.Id;
            c.Client = ToClientDto(i.Client);
            c.Product = ToProductDto(i.Product);
            c.CreationDate = i.CreationDate;
            c.StartRentDate = i.StartRentDate;
            c.EndRentDate = i.EndRentDate;              
            c.Status = ToRentalStatusDto(i.RentalStatu);
            c.Description = i.Description;
            return c;
        }

        private Rental ToRental(Rental rental, Client client, Product product, RentalStatu status, RentalDto i)
        {
            Rental p = rental;
            p.Id = i.Id;
            p.Client = client;
            p.Product = product;
            p.ClientId = i.Client.Id;
            p.ProductId = i.Product.Id;
            p.StatusId = i.Status.Id;
            p.CreationDate = i.CreationDate;
            p.StartRentDate = i.StartRentDate;
            p.EndRentDate = i.EndRentDate;
            p.RentalStatu = status;
            p.Description = i.Description;
            return p;
        }

        public RentalStatusDto GetRentalStatusById(int id)
        {
            return ToRentalStatusDto(entities.RentalStatus.FirstOrDefault(i => i.Id == id));
        }

        public IEnumerable<RentalStatusDto> GetAllRentalStatuses()
        {
            var list1 = entities.RentalStatus.ToList();

            List<RentalStatusDto> list2 = new List<RentalStatusDto>();
            foreach (var i in list1)
            {
                list2.Add(ToRentalStatusDto(i));
            }

            return list2;
        }

        public void AddRentalStatus(RentalStatusDto rentalstatus)
        {
            entities.RentalStatus.Add(ToRentalStatus(new RentalStatu(), rentalstatus));
            entities.SaveChanges();
        }

        public void RemoveRentalStatus(RentalStatusDto rentalstatus)
        {
            var rs = entities.RentalStatus.First(a => a.Id == rentalstatus.Id);

            RentalStatu ren = ToRentalStatus(rs, rentalstatus);
            entities.RentalStatus.Attach(ren);
            entities.Entry(ren).State = EntityState.Deleted;
            entities.SaveChanges();
        }

        public void UpdateRentalStatus(RentalStatusDto rentalstatus)
        {
            var rs = entities.RentalStatus.First(a => a.Id == rentalstatus.Id);

            entities.Entry(ToRentalStatus(rs, rentalstatus)).State = EntityState.Modified;
            entities.SaveChanges();
        }

        private RentalStatusDto ToRentalStatusDto(RentalStatu i)
        {
            RentalStatusDto c = new RentalStatusDto();
            c.Id = i.Id;
            c.Nazwa = i.Nazwa;
            return c;
        }

        private RentalStatu ToRentalStatus(RentalStatu r, RentalStatusDto i)
        {
            RentalStatu p = r;
            p.Id = i.Id;
            p.Nazwa = i.Nazwa;
            return p;
        }
    }
}
