﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wcf.DTOs
{
    [DataContract]
    public class RentalStatusDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Nazwa { get; set; }
    }
}
