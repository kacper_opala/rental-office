﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RentalOffice.Wcf.DTOs
{
    [DataContract]
    public class RentalDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public ClientDto Client { get; set; }
        [DataMember]
        public ProductDto Product { get;set; }
        [DataMember]
        public System.DateTime CreationDate { get; set; }
        [DataMember]
        public System.DateTime? StartRentDate { get; set; }
        [DataMember]
        public System.DateTime? EndRentDate { get; set; }
        [DataMember]
        public RentalStatusDto Status { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
